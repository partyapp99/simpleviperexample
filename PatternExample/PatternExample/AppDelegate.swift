//
//  AppDelegate.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window:UIWindow?

        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let navigationController = UINavigationController()
            window?.rootViewController = navigationController
            let coordinator = MainCoordinator(with: navigationController)
            coordinator.start()
            window?.makeKeyAndVisible()

            return true
        }
}

