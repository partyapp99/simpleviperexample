//
//  ViewController.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import UIKit

class ViewController: UIViewController,HomeViewControllerProtocol, Storyboarded {
    static var storyboardName: String = "Main"
    @IBOutlet weak var collectionView: UICollectionView!
    var viewState: HomeUIModel = HomeUIModel(photos: []) {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.updateView()
            }
        }
    }
    var viewModel : HomeViewModelProtocol!
    var dataSource : PhotoCollectionDataSource!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        viewModel.viewIsReady()
        // Do any additional setup after loading the view.
    }
    
    private func setupDataSource() {
        dataSource = PhotoCollectionDataSource(with: collectionView)
        dataSource.relaodCollection = {
            self.collectionView.reloadData()
        }
        dataSource.cellTapped = { value in
            self.viewModel.cellTapped(with: value)
        }
        self.collectionView.dataSource = dataSource
        self.collectionView.delegate = dataSource
    }
    
    func showLoader() {
        
    }
    
    func hideLoader() {
        
    }
    
    private func updateView() {
        dataSource.items = viewState.photos
    }


}

