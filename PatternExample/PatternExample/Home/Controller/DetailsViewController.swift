//
//  DetailsViewController.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
import UIKit
class DetailsViewController : UIViewController, Storyboarded, DetailsViewControllerProtocol {
    static var storyboardName: String = "Main"
    @IBOutlet weak var image: UIImageView!
    var viewState: DetailUIModel = DetailUIModel(photo: "", title: "") {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.updateView()
            }
        }
    }
    var viewModel : DetailsViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewIsReady()
    }
    
    
    
    
    private func updateView() {
        self.title = viewState.title
        self.image.downloaded(from: viewState.photo)
        
    }
    func showLoader() {
        
    }
    
    func hideLoader() {
        
    }
    
    
}
