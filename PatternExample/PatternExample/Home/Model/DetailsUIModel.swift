//
//  DetailsUIModel.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
struct DetailUIModel {
    var photo: String
    var title: String
}
