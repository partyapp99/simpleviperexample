//
//  HomeUIModel.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation

struct HomeUIModel{
    var photos: [PhotoResponseElement]
}
