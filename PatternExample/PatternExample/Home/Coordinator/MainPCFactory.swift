//
//  MainPCFactory.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation

typealias HomeRoot = (controller: ViewController, viewModel : HomeViewModel)
typealias DetailsPC = (controller: DetailsViewController, viewModel : DetailsViewModel)

struct MainPCFactory {
    func homeRoot() -> HomeRoot {
        let intercator = HomeIntercator(with: HomeClient())
        let controller = ViewController.instantiate()
        let viewModel = HomeViewModel(with: controller)
        controller.viewModel = viewModel
        viewModel.interactor = intercator
        return (controller: controller, viewModel: viewModel)
    }
    
    func detailsPC() -> DetailsPC {
        let controller = DetailsViewController.instantiate()
        let viewModel = DetailsViewModel(with: controller)
        controller.viewModel = viewModel
        return (controller: controller, viewModel: viewModel)
    }
}
