//
//  MainCoordinator.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
import UIKit

class MainCoordinator : BaseCoordinator {
    var child: [BaseCoordinator] = []
    var factory = MainPCFactory()
    var navigationController: UINavigationController
    
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let pcHome = factory.homeRoot()
        pcHome.viewModel.nextClosure = { title, photo in
            self.goToDetails(with: DetailUIModel(photo: photo, title: title))
        }
        self.navigationController.pushViewController(pcHome.controller, animated: true)
    }
    
    func goToDetails(with model: DetailUIModel) {
        let detailsPC = factory.detailsPC()
        detailsPC.viewModel.viewState = model
        self.navigationController.pushViewController(detailsPC.controller, animated: true)
    }
}
