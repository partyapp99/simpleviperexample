//
//  HomeViewModel.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
import UIKit
class HomeViewModel : HomeViewModelProtocol {
    typealias HomeViewController = HomeViewControllerProtocol & UIViewController
    var viewState : HomeUIModel = HomeUIModel(photos: [])
    var view: HomeViewController?
    var interactor: HomeViewIntercatorProtocol?
    var nextClosure: (String,String) -> Void = { _ ,_ in}
    func viewIsReady() {
        interactor?.getPhoto(completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let photos):
                self.viewState.photos = photos
                self.view?.viewState = self.viewState
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
    func viewWillApper() {
        
    }
    
    func viewDidApper() {
        
    }
    
    func cellTapped(with indexPath: IndexPath) {
        let item = self.viewState.photos[indexPath.row]
        guard let title = item.user?.username, let imageUrl = item.urls?.regular else { return }
        self.nextClosure(title,imageUrl)
    }
    
    required init(with view: UIViewController) {
        self.view = view as? HomeViewModel.HomeViewController
    }
    
    
}
