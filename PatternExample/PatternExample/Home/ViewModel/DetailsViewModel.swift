//
//  DetailsViewModel.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
import UIKit
class DetailsViewModel : DetailsViewModelProtocol {
    typealias DetailsView = DetailsViewControllerProtocol & UIViewController
    var view: DetailsView?
    var viewState : DetailUIModel = DetailUIModel(photo: "", title: "")
    func viewIsReady() {
        view?.viewState = viewState
    }
    
    func viewWillApper() {
        
    }
    
    func viewDidApper() {
        
    }
    
    required init(with view: UIViewController) {
        self.view = view as! DetailsViewModel.DetailsView
    }
    
    
}
