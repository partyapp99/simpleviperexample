//
//  HomeInteractor.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation

class HomeIntercator : HomeViewIntercatorProtocol{
    var client : HomeServiceProtocol!
    init(with client: HomeServiceProtocol) {
        self.client = client
    }
    func getPhoto(completion: @escaping ((Result<PhotoResponse, Error>) -> Void)) {
        client.getPhoto { result in
            completion(result)
        }
    }
}
