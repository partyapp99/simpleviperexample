//
//  HomeContracts.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation

protocol HomeViewModelProtocol : BaseViewModel {
    func cellTapped(with indexPath: IndexPath)
}

protocol HomeViewControllerProtocol : BaseController {
    var viewState: HomeUIModel { get set }
}

protocol HomeViewIntercatorProtocol {
    func getPhoto(completion: @escaping((Result<PhotoResponse, Error>) -> Void))
}

