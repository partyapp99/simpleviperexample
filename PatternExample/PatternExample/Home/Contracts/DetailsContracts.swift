//
//  DetailsContracts.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation

protocol DetailsViewModelProtocol : BaseViewModel {
    
}

protocol DetailsViewControllerProtocol: BaseController {
    var viewState: DetailUIModel { get set }
}
