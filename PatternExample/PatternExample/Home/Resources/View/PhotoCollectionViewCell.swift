//
//  PhotoCollectionViewCell.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    final func setup(with element: PhotoResponseElement) {
        photo.downloaded(from: element.urls?.thumb ?? "")
        
    }

}
