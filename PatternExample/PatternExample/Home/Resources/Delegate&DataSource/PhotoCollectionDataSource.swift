//
//  PhotoCollectionDataSource.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
import UIKit

class PhotoCollectionDataSource: NSObject, UICollectionViewDataSource,CellDequeuer,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    var relaodCollection: () -> Void = { }
    var items: [PhotoResponseElement] = [] {
        didSet {
            DispatchQueue.main.async {
                self.relaodCollection()
            }
        }
    }
    
    var cellTapped: (IndexPath) -> () = { _ in }
    
    init(with collectionView: UICollectionView) {
        collectionView.registerNibWithClassType(type: PhotoCollectionViewCell.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : PhotoCollectionViewCell = dequeueCell(collectionView: collectionView, indexPath: indexPath)
        cell.setup(with: items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.cellTapped(indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width / 2 - 5), height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
