//
//  HomeServiceProtocol.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
protocol HomeServiceProtocol {
    func getPhoto(completion: @escaping((Result<PhotoResponse, Error>) -> Void))
}
