//
//  HomeClient.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation

class HomeClient : HomeServiceProtocol {
    var key = "396ee54513df9487a4b9578491ed8d15b9d506f9f8f61c45dd250a078a241545"
    func getPhoto(completion: @escaping ((Result<[PhotoResponseElement], Error>) -> Void)) {
        let baseUrl = "https://api.unsplash.com/photos/?client_id=\(key)"
        guard let url = URL(string: baseUrl) else { return }
        let session = URLSession(configuration: .default).dataTask(with: url) { data, response, error in
            if let err = error {
                completion(.failure(err))
            }
            do {
                if let d = data , let jsonResponse = try? JSONDecoder().decode([PhotoResponseElement].self, from: d) {
                    completion(.success(jsonResponse))
                }
            }
        }
        session.resume()
    }
    
    
}
