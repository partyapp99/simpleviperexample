// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let photoResponse = try? newJSONDecoder().decode(PhotoResponse.self, from: jsonData)

import Foundation

// MARK: - PhotoResponseElement
class PhotoResponseElement: Codable {
    var id: String?
    var createdAt: Date?
    var updatedAt: Date?
    var promotedAt: Date?
    var width: Int?
    var height: Int?
    var color: String?
    var blurHash: String?
    var photoResponseDescription: String?
    var altDescription: String?
    var urls: Urls?
    var links: PhotoResponseLinks?
    var categories: [JSONAny]?
    var likes: Int?
    var likedByUser: Bool?
    var currentUserCollections: [JSONAny]?
    var sponsorship: Sponsorship?
    var user: User?

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt
        case updatedAt
        case promotedAt
        case width
        case height
        case color
        case blurHash
        case photoResponseDescription
        case altDescription
        case urls
        case links
        case categories
        case likes
        case likedByUser
        case currentUserCollections
        case sponsorship
        case user
    }

    init(id: String?, createdAt: Date?, updatedAt: Date?, promotedAt: Date?, width: Int?, height: Int?, color: String?, blurHash: String?, photoResponseDescription: String?, altDescription: String?, urls: Urls?, links: PhotoResponseLinks?, categories: [JSONAny]?, likes: Int?, likedByUser: Bool?, currentUserCollections: [JSONAny]?, sponsorship: Sponsorship?, user: User?) {
        self.id = id
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.promotedAt = promotedAt
        self.width = width
        self.height = height
        self.color = color
        self.blurHash = blurHash
        self.photoResponseDescription = photoResponseDescription
        self.altDescription = altDescription
        self.urls = urls
        self.links = links
        self.categories = categories
        self.likes = likes
        self.likedByUser = likedByUser
        self.currentUserCollections = currentUserCollections
        self.sponsorship = sponsorship
        self.user = user
    }
}

// MARK: - PhotoResponseLinks
class PhotoResponseLinks: Codable {
    var linksSelf: String?
    var html: String?
    var download: String?
    var downloadLocation: String?

    enum CodingKeys: String, CodingKey {
        case linksSelf
        case html
        case download
        case downloadLocation
    }

    init(linksSelf: String?, html: String?, download: String?, downloadLocation: String?) {
        self.linksSelf = linksSelf
        self.html = html
        self.download = download
        self.downloadLocation = downloadLocation
    }
}

// MARK: - Sponsorship
class Sponsorship: Codable {
    var impressionUrls: [String]?
    var tagline: String?
    var taglineUrl: String?
    var sponsor: User?

    enum CodingKeys: String, CodingKey {
        case impressionUrls
        case tagline
        case taglineUrl
        case sponsor
    }

    init(impressionUrls: [String]?, tagline: String?, taglineUrl: String?, sponsor: User?) {
        self.impressionUrls = impressionUrls
        self.tagline = tagline
        self.taglineUrl = taglineUrl
        self.sponsor = sponsor
    }
}

// MARK: - User
class User: Codable {
    var id: String?
    var updatedAt: Date?
    var username: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var twitterUsername: String?
    var portfolioUrl: String?
    var bio: String?
    var location: String?
    var links: UserLinks?
    var profileImage: ProfileImage?
    var instagramUsername: String?
    var totalCollections: Int?
    var totalLikes: Int?
    var totalPhotos: Int?
    var acceptedTos: Bool?
    var forHire: Bool?
    var social: Social?

    enum CodingKeys: String, CodingKey {
        case id
        case updatedAt
        case username
        case name
        case firstName
        case lastName
        case twitterUsername
        case portfolioUrl
        case bio
        case location
        case links
        case profileImage
        case instagramUsername
        case totalCollections
        case totalLikes
        case totalPhotos
        case acceptedTos
        case forHire
        case social
    }

    init(id: String?, updatedAt: Date?, username: String?, name: String?, firstName: String?, lastName: String?, twitterUsername: String?, portfolioUrl: String?, bio: String?, location: String?, links: UserLinks?, profileImage: ProfileImage?, instagramUsername: String?, totalCollections: Int?, totalLikes: Int?, totalPhotos: Int?, acceptedTos: Bool?, forHire: Bool?, social: Social?) {
        self.id = id
        self.updatedAt = updatedAt
        self.username = username
        self.name = name
        self.firstName = firstName
        self.lastName = lastName
        self.twitterUsername = twitterUsername
        self.portfolioUrl = portfolioUrl
        self.bio = bio
        self.location = location
        self.links = links
        self.profileImage = profileImage
        self.instagramUsername = instagramUsername
        self.totalCollections = totalCollections
        self.totalLikes = totalLikes
        self.totalPhotos = totalPhotos
        self.acceptedTos = acceptedTos
        self.forHire = forHire
        self.social = social
    }
}

// MARK: - UserLinks
class UserLinks: Codable {
    var linksSelf: String?
    var html: String?
    var photos: String?
    var likes: String?
    var portfolio: String?
    var following: String?
    var followers: String?

    enum CodingKeys: String, CodingKey {
        case linksSelf
        case html
        case photos
        case likes
        case portfolio
        case following
        case followers
    }

    init(linksSelf: String?, html: String?, photos: String?, likes: String?, portfolio: String?, following: String?, followers: String?) {
        self.linksSelf = linksSelf
        self.html = html
        self.photos = photos
        self.likes = likes
        self.portfolio = portfolio
        self.following = following
        self.followers = followers
    }
}

// MARK: - ProfileImage
class ProfileImage: Codable {
    var small: String?
    var medium: String?
    var large: String?

    enum CodingKeys: String, CodingKey {
        case small
        case medium
        case large
    }

    init(small: String?, medium: String?, large: String?) {
        self.small = small
        self.medium = medium
        self.large = large
    }
}

// MARK: - Social
class Social: Codable {
    var instagramUsername: String?
    var portfolioUrl: String?
    var twitterUsername: String?
    var paypalEmail: JSONNull?

    enum CodingKeys: String, CodingKey {
        case instagramUsername
        case portfolioUrl
        case twitterUsername
        case paypalEmail
    }

    init(instagramUsername: String?, portfolioUrl: String?, twitterUsername: String?, paypalEmail: JSONNull?) {
        self.instagramUsername = instagramUsername
        self.portfolioUrl = portfolioUrl
        self.twitterUsername = twitterUsername
        self.paypalEmail = paypalEmail
    }
}

// MARK: - Urls
class Urls: Codable {
    var raw: String?
    var full: String?
    var regular: String?
    var small: String?
    var thumb: String?

    enum CodingKeys: String, CodingKey {
        case raw
        case full
        case regular
        case small
        case thumb
    }

    init(raw: String?, full: String?, regular: String?, small: String?, thumb: String?) {
        self.raw = raw
        self.full = full
        self.regular = regular
        self.small = small
        self.thumb = thumb
    }
}

typealias PhotoResponse = [PhotoResponseElement]

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
