//
//  HomeClientMock.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation

class MockManager {
    class func jsonFile(with name: String) -> Data? {
        if let path = Bundle.main.path(forResource: name, ofType: "json") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
              } catch {
                   // handle error
                  return nil
              }
        }
        return nil
    }
}
class HomeClientMock : HomeServiceProtocol {
    func getPhoto(completion: @escaping ((Result<PhotoResponse, Error>) -> Void)) {
        if let data = MockManager.jsonFile(with: "photoMock"), let jsonResponse = try? JSONDecoder().decode([PhotoResponseElement].self, from: data) {
            completion(.success(jsonResponse))
        }
        else {
            completion(.success([]))
        }
    }
    
}
