//
//  Storyboarded.swift
//  Pagoda
//
//  Created by Simonelli Lorenzo on 19/09/2020.
//  Copyright © 2020 Simonelli Lorenzo. All rights reserved.
//

import UIKit

protocol Storyboarded {
    static var storyboardName: String { get set }

    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate() -> Self {
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(self)

        // this splits by the dot and uses everything after, giving "MyViewController"
        let components: [String] = fullName.components(separatedBy: ".")
        let className = components[1]

        // load our storyboard
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)

        // instantiate a view controller with that identifier, and force cast as the type that was requested
        guard let viewController = storyboard.instantiateViewController(withIdentifier: className) as? Self else {
            fatalError("Storyboard ID must have same class name of view controller: \(className)")
        }

        return viewController
    }
}
