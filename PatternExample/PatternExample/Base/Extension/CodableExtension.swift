//
//  StructExtension.swift
//  BancoPosta
//
//  Created by Antonio Scardigno on 23/09/2019.
//  Copyright © 2019 Poste Italiane S.P.A. All rights reserved.
//

import Foundation

extension Encodable {
    
    var asDictionary: [String: Any] {
        
        guard let dictionary = try? DictionaryEncoder().encode(self) else {
            return [:]
        }
        
        return dictionary
    }
}

extension Decodable {
    
    init?(any: Any?) {
        if let dictionary = any as? [String: Any] {
            do {
                let value = try DictionaryDecoder().decode(Self.self, from: dictionary)
                self = value
            } catch {
                print(error)
                return nil
            }
        } else {
            print("Not a [String:Any] dictionary")
            return nil
        }
    }
    
    init(from value: Any) throws {
        guard let dictionary = value as? [String: Any] else {
            throw NSError.init(domain: "Parsing", code: -1, userInfo: ["descrizione": "Questo valore da decodificare non è un dizionario."])
        }
        self = try DictionaryDecoder().decode(Self.self, from: dictionary)
    }
}

extension Array where Element: Decodable {
    init(dictArray: [[String: Any]]) {
        let value = dictArray.compactMap({
            try? Element(from: $0)
        })
        self = value
    }
}

extension Dictionary where Dictionary.Key == AnyHashable, Dictionary.Value == Any {
    func asValue<T>() -> T? where T: Decodable {
        return T(any: self)
    }
}
