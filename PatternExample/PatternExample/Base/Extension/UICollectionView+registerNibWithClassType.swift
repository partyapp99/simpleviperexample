//
//  UICollectionView+registerNibWithClassType.swift
//  BancoPosta
//
//  Created by Simonelli Lorenzo on 20/10/2020.
//  Copyright © 2020 Poste Italiane S.P.A. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    func registerNibWithClassType<T: AnyObject>(type _: T.Type) {
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(T.self)

        // this splits by the dot and uses everything after, giving "MyViewController"
        let className = fullName.components(separatedBy: ".")[1]

        register(UINib(nibName: className, bundle: nil), forCellWithReuseIdentifier: className)
    }

    func registerReusableViewNibWithClassType<T: AnyObject>(type _: T.Type) {
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(T.self)

        // this splits by the dot and uses everything after, giving "MyViewController"
        let className = fullName.components(separatedBy: ".")[1]

        let nib = UINib(nibName: className, bundle: nil)
        register(nib,
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                 withReuseIdentifier: className)
    }
}
