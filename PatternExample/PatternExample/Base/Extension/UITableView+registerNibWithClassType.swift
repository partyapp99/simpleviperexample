//
//  UITableView+registerNibWithClassType.swift
//  BancoPosta
//
//  Created by Simonelli Lorenzo on 16/10/2020.
//  Copyright © 2020 Poste Italiane S.P.A. All rights reserved.
//

import UIKit

extension UITableView {
    func registerNibWithClassType<T: AnyObject>(type _: T.Type) {
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(T.self)

        // this splits by the dot and uses everything after, giving "MyViewController"
        let className = fullName.components(separatedBy: ".")[1]

        register(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: className)
    }
}
