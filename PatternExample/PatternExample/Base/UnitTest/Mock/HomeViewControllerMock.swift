//
//  HomeViewControllerMock.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//
@testable import PatternExample
import Foundation
import UIKit


class HomeViewControllerMock : UIViewController, HomeViewControllerProtocol {
    var viewState: HomeUIModel = HomeUIModel(photos: [])
    var viewModel: HomeViewModelProtocol!
    
    func detailsTapped(with index: IndexPath) {
        viewModel.cellTapped(with: index)
    }
    
    func showLoader() {
    }
    
    func hideLoader() {
    }
    
    
}
