//
//  HomeUnitTest.swift
//  PatternExampleTests
//
//  Created by Francesco Stabile on 27/07/21.
//
@testable import PatternExample
import XCTest

class HomeUnitTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testIfViewIsReady() {
        let controller = HomeViewControllerMock()
        let intercator = HomeIntercator(with: HomeClientMock())
        let viewModel = HomeViewModel(with: controller)
        viewModel.interactor = intercator
        controller.viewModel = viewModel
        viewModel.viewIsReady()
        XCTAssertEqual(controller.viewState.photos.count > 0, true)
    }
    
    
    func testCellTapped() {
        let controller = HomeViewControllerMock()
        let intercator = HomeIntercator(with: HomeClientMock())
        let viewModel = HomeViewModel(with: controller)
        viewModel.interactor = intercator
        controller.viewModel = viewModel
        viewModel.viewIsReady()
        var title = ""
        viewModel.nextClosure = { t, photo in
            title = t
        }
        controller.detailsTapped(with: IndexPath(item: 0, section: 0))
        XCTAssertEqual(title, controller.viewState.photos.first?.user?.username)
        
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
