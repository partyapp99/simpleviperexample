//
//  BaseProtocols.swift
//  PatternExample
//
//  Created by Francesco Stabile on 27/07/21.
//

import Foundation
import UIKit

protocol BaseViewModel {
    func viewIsReady()
    func viewWillApper()
    func viewDidApper()
    init(with view: UIViewController)
}

protocol BaseController {
    func showLoader()
    func hideLoader()
}


protocol BaseCoordinator {
    var child: [BaseCoordinator] { get set }
    var navigationController: UINavigationController { get set}
}
